sshd
=========

Installs and configure Openssh-server

Requirements
------------

none

Role Variables
--------------

See [defaults/main.yml](defaults/main.yml), [vars/main.yml](vars/main.yml) and [templates/sshd_config.conf.j2](templates/sshd_config.conf.j2)

Dependencies
------------

none

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: cinux.sshd }

License
-------

MIT